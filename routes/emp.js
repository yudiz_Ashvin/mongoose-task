const express = require('express')
const router = express.Router()
const EmployeeController = require('../controllers/employee')
router.get('/', EmployeeController.index)
router.post('/read', EmployeeController.show)
router.post('/create', EmployeeController.store)
router.post('/update', EmployeeController.update)
router.post('/delete', EmployeeController.destroy)
module.exports = router