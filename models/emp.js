const mongoose = require('mongoose')
const Schema = mongoose.Schema
const employee = new Schema({
    name: {
        type: String,
    },
    place: {
        type: String
    },
    email: {
        type: String
    },
    phone: {
        type: Number
    },
    age: {
        type: Number
    }
}, {
    timestamps: true
})
const Emp = mongoose.model('Employee', employee)
module.exports = Emp