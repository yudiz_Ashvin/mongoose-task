const { response } = require('express')
const Employee = require('../models/emp')
const index = (req, res, next) => {
    Employee.find()
        .then(response => {
            res.json({
                response
            })
        })
        .catch(error => {
            res.json({
                message: 'An error Occured?'
            })
        })
}

const show = (req, res, next) => {
    let employeeID = req.body.employeeID
    Employee.findById(employeeID)
        .then(response => {
            res.json({
                response
            })
        })
        .catch(error => {
            res.json({
                message: 'An error Occured?'
            })
        })
}

const store = (req, res, next) => {
    let employee = new Employee({
        name: req.body.name,
        place: req.body.place,
        email: req.body.email,
        phone: req.body.phone,
        age: req.body.age
    })
    employee.save()
        .then(response => {
            res.json({
                message: 'Success'
            })
        })
        .catch(error => {
            res.json({
                message: 'An error Occured?'
            })
        })
}

const update = (req, res, next) => {
    let employeeID = req.body.employeeID
    let updateData = {
        name: req.body.name,
        place: req.body.place,
        email: req.body.email,
        phone: req.body.phone,
        age: req.body.age
    }
    Employee.findByIdAndUpdate(employeeID, { $set: updateData })
        .then(response => {
            res.json({
                message: 'Update Success'
            })
        })
        .catch(err => {
            res.json({
                message: 'An error Occured?'
            })
        })
}
const destroy = (req, res, next) => {
    let employeeID = req.body.employeeID
    Employee.findByIdAndRemove(employeeID)
        .then(response => {
            res.json({
                message: 'Delete Success'
            })
        })
        .catch(error => {
            res.json({
                message: 'An error Occured?'
            })
        })
}
module.exports = {
    index,
    show,
    store,
    update,
    destroy
}